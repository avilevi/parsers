package com.example

import avi.parsers.conditional.ASTObjects._
import avi.parsers.conditional.ConditionParser

import avi.typeclass.example._

object Hello {
  def main(args: Array[String]): Unit = {
    println("Hello, json!")


      implicit val astExpressionToJSON = new JSONSerializer[AST] {

        override def toJSON(value: AST):JSON = value match{

          case BooleanOperation(op, lhs, rhs) => JObj(
          Map(
          "Operation"-> JStr(op),
          "lhs" -> toJSON(lhs),
          "rhs" -> toJSON(rhs)
          ))
        case  LRComparison (lhs: Variable, op, rhs: Constant) =>JObj(
          Map(
            "Operation"-> JStr(op),
            "lhs" -> toJSON(lhs),
            "rhs" -> toJSON(rhs)
          ))
        case  Comparison(lhs,op) =>JObj(
          Map(
            "Operation"-> JStr(op),
            "lhs" -> toJSON(lhs)
          ))

        case  NumericConstant(v) => JNum(v)

        case  DateConstant(d) => JStr(d.toString)

        case  StringConstant(str) => JStr(str)

        case  EmptyConstant => JNull

        case Variable(n) => JStr(n)
      }

    }


    val parser = new ConditionParser {}
    val p:AST = parser.parse("foo = '2015-03-25' || bar = 8").get //I know that the get will succeed here
    val s = JSONWriter.write(p)
    val s1 = JSONWriter.toJSONString(p)
    println(s)
    assert(s==s1)


    //option 2 adding functionality to external class
    import JSONConverterImplicits._
    val p2 = parser.parse("foo = '2015-03-25' || bar = 8 ").get
    val s2 = JSONWriter.write(p2.asJSON)
    println(s2)
    assert(s==s2)

    import PersonImplicits._
    val person = Person("Drakula",578)
    println(JSONWriter.write(person))



  }
}
