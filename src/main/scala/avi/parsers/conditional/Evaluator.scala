package avi.parsers.conditional

import avi.parsers.conditional.ASTObjects._


trait Evaluator{
  import FilterConstants._
  def evaluate(expression:BooleanExpression)(implicit env: Map[String, Constant]) : Boolean = expression match {

    case LRComparison(v:Variable, Equals, c) =>  (v.eval(env),c) match{
      case (StringConstant(l),StringConstant(r)) => l == r
      case (NumericConstant(l),NumericConstant(r)) => l == r
      case (StringConstant(l),NumericConstant(r)) => r == l.toDouble
      case (a,b) =>
        println(s"FAILED to evaluate '==' got $a not equal to $b" )
        false
    }
    case LRComparison(v:Variable, NotEquals, c) =>  (v.eval(env),c) match{
      case (StringConstant(l),StringConstant(r)) => l != r
      case (NumericConstant(l),NumericConstant(r)) => l != r
      case (StringConstant(l),NumericConstant(r)) =>  r != l.toDouble
      case (a,b) =>
        println(s"FAILED to evaluate != got $a not equal to $b" )
        false
    }
    case LRComparison(v:Variable, Greater, c) =>  (v.eval(env),c) match{
      case (NumericConstant(l),NumericConstant(r)) => l > r
      case (StringConstant(l),NumericConstant(r)) => l.toDouble > r
      case (a,b) =>
        println(s"FAILED to evaluate $Greater got $a not equal to $b" )
        false
    }
    case LRComparison(v:Variable, Smaller, c) =>  (v.eval(env),c) match{
      case (NumericConstant(l),NumericConstant(r)) => l < r
      case (StringConstant(l),NumericConstant(r)) => l.toDouble < r
      case (a,b) =>
        println(s"FAILED to evaluate $Smaller got $a not equal to $b" )
        false
    }
    case LRComparison(v:Variable, SmallerOrEqual, c) =>  (v.eval(env),c) match{
      case (NumericConstant(l),NumericConstant(r)) => l <= r
      case (StringConstant(l),NumericConstant(r)) => l.toDouble <= r
      case (a,b) =>
        println(s"FAILED to evaluate $SmallerOrEqual got $a not equal to $b" )
        false
    }
    case LRComparison(v:Variable, GreaterOrEqual, c) =>  (v.eval(env),c) match{
      case (NumericConstant(l),NumericConstant(r)) =>
        l >= r
      case (StringConstant(l),NumericConstant(r)) =>
        l.toDouble >= r
      case (a,b) =>
        println(s"FAILED to evaluate $GreaterOrEqual got $a not equal to $b" )
        false
    }
    case Comparison(v:Variable,IsNotNull) => v.eval(env) match{
      case EmptyConstant => false
      case _ => true
    }
    case Comparison(v:Variable,IsNull) => v.eval(env) match{
      case EmptyConstant => true
      case _ => false
    }

    case BooleanOperation(UnaryOr, a, b) => evaluate(a) || evaluate(b)
    case BooleanOperation(OR, a, b) => evaluate(a) || evaluate(b)
    case BooleanOperation(UnaryAnd, a, b) => evaluate(a) && evaluate(b)
    case BooleanOperation(And, a, b) => evaluate(a) && evaluate(b)
  }
}