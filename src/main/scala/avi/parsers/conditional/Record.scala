package avi.parsers.conditional


case class Record(fields:Map[String,String]){
  def getFieldValue(name:String) = fields(name)
}