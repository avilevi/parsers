package avi.parsers.conditional

object ASTObjects {
  sealed abstract class AST

  sealed trait BooleanExpression extends AST

  sealed trait Constant extends AST

  case class BooleanOperation(op: String, lhs: BooleanExpression, rhs: BooleanExpression) extends BooleanExpression

  case class Comparison(lhs: Constant,op: String) extends BooleanExpression

  case class LRComparison (lhs: Variable, op: String, rhs: Constant) extends BooleanExpression

  case class NumericConstant(value: Double) extends Constant

  case class DateConstant(value: Long) extends Constant

  case class StringConstant(value: String) extends Constant

  case object EmptyConstant extends Constant

  case class Variable(name: String) extends Constant {
    def eval(env: Map[String, Constant]) = env(name)

    override def toString = name
  }
}
