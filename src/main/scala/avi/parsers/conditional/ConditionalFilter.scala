package avi.parsers.conditional

import avi.parsers.conditional.ASTObjects._


class ConditionalFilter(ruleText:String) extends ConditionParser with Evaluator{

  val rule = parse(ruleText)
  val fields = rule match {
    case Some(r) => extractFields(r)
    case _ => Nil
  }

  def extractFields(b:BooleanExpression):List[String] = b match {
    case BooleanOperation(_,l,r) => extractFields(l):::extractFields(r)
    case LRComparison(l,_,_) => List(l.toString)
    case Comparison(l,_) => List(l.toString)
    case _ => Nil
  }

  def getMap(record:Record,fields:List[String]):Map[String, Constant] = {
    fields.foldLeft(Map.empty[String, Constant]){(m,f) =>
      m + (f -> {
        Option(record.getFieldValue(f)) match {
          case Some(v) =>
            StringConstant (v.toString)
          case _ => EmptyConstant
        }
      })
    }
  }

  def filter(record:Record ):Option[Record] = {
    implicit val m = getMap(record,fields)
    if (rule.exists(evaluate)){
      Some(record)
    }
    else
      None
  }

}
