package avi.parsers.conditional

import java.sql.{Timestamp, Date}
import java.text.SimpleDateFormat
import java.time.format.DateTimeFormatter

import avi.parsers.conditional.ASTObjects._

import scala.util.Try
import scala.util.parsing.combinator.{JavaTokenParsers, PackratParsers}

object FilterConstants{
  val And = "AND"
  val OR = "OR"
  val UnaryAnd = "&&"
  val UnaryOr = "||"
  val Equals = "="
  val NotEquals = "!="
  val Greater = ">"
  val Smaller = "<"
  val GreaterOrEqual = ">="
  val SmallerOrEqual = "<="
  val IsNull = "IS NULL"
  val IsNotNull = "IS NOT NULL"
}
trait ConditionParser extends JavaTokenParsers with PackratParsers {
  import FilterConstants._
  override def stringLiteral: Parser[String] =  """(["'])[^"']*\1""".r
  val  simpleDateFormat:SimpleDateFormat = new SimpleDateFormat("yyyy-mm-dd")
  val  longDateTimeFormat:SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS")
  val  shortDateTimeFormat:SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")

  val d = """'+\d{4}-\d{2}-\d{2}+'""".r // "'2015-03-25'".matches("""'+\d{4}-\d{2}-\d{2}+'""")
  def dateLiteral: Parser[String] = d
  val sts = """'\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2}'""".r
  val lts = """'\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2}.\d+'""".r
  //  val catchAll = """'\d{4}-\d{2}-\d{2}(\s\d{2}:\d{2}:\d{2}(.\d+)*)*'""".r //
  def longDateTimeLiteral: Parser[String] = lts
  def shortDateTimeLiteral: Parser[String] = sts
  implicit class QuotesOps(s:String) {
    val removeSingleQuote: String = s.replaceAll("'", "")
  }
  val booleanOperator : PackratParser[String] = literal(UnaryOr) | literal(UnaryAnd) | literal(And) | literal(OR)
  val comparisonOperator : PackratParser[String] = literal(SmallerOrEqual) | literal(GreaterOrEqual) | literal(Equals) | literal(NotEquals) | literal(Smaller) | literal(Greater)
  val nullCompare : PackratParser[String] = literal(IsNotNull) | literal(IsNull)
  val constant : PackratParser[Constant] = floatingPointNumber ^^ { x => NumericConstant(x.toDouble) } |
    dateLiteral  ^^ {d => DateConstant(simpleDateFormat.parse(d.removeSingleQuote).getTime)}  |
    shortDateTimeLiteral  ^^ {d => DateConstant(shortDateTimeFormat.parse(d.removeSingleQuote).getTime)}  |
    longDateTimeLiteral  ^^ {d => DateConstant(longDateTimeFormat.parse(d.removeSingleQuote).getTime)}  |
    stringLiteral ^^ {s => StringConstant(s.replaceAll("'","")) }
  val variable = ident ^^ {s=>Variable(s)}

  val comparison : PackratParser[BooleanExpression] = (variable ~ comparisonOperator ~ constant) ^^ { case lhs ~ op ~ rhs => LRComparison(lhs, op, rhs) } |
    (variable ~ nullCompare) ^^ {case lhs ~ nc => Comparison(lhs,nc)}

  lazy val p1 : PackratParser[BooleanExpression] = booleanOperation | comparison
  val booleanOperation : PackratParser[BooleanExpression]= (p1 ~ booleanOperator ~ p1) ^^ { case lhs ~ op ~ rhs => BooleanOperation(op, lhs, rhs) }|
    "(" ~> booleanOperation <~ ")"

  def parse(text:String) :Option[BooleanExpression] = {
    val res = parseAll(p1, text)
    res match {
      case Success(r, n) => Some(r)
      case Failure(msg, n) =>
        println("Failure parsing "+ text+"\nMessage: " +msg)
        None
      case Error(msg, n) =>
        println("Error"+msg)
        None
    }
  }
}
