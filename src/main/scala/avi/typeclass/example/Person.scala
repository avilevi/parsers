package avi.typeclass.example

case class Person (name:String,age:Int)

object PersonImplicits{
  implicit val personToJSON = {
    new JSONSerializer[Person] {
       override def toJSON(value: Person): JSON =JObj( Map("name" -> JStr(value.name),
       "age" -> JNum(value.age)))
    }
  }
}