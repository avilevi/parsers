package avi.typeclass.example

import avi.parsers.conditional.ASTObjects._

object JSONConverterImplicits {
  implicit class ASTConverter(ast:AST){
    def asJSON: JSON = ast match{

      case BooleanOperation(op, lhs, rhs) => JObj(
        Map(
          "Operation"-> JStr(op),
          "lhs" -> lhs.asJSON,
          "rhs" -> rhs.asJSON
        ))
      case  LRComparison (lhs: Variable, op, rhs: Constant) =>JObj(
        Map(
          "Operation"-> JStr(op),
          "lhs" -> lhs.asJSON,
          "rhs" -> rhs.asJSON
        ))
      case  Comparison(lhs,op) =>JObj(
        Map(
          "Operation"-> JStr(op),
          "lhs" -> lhs.asJSON
        ))
      case  NumericConstant(v) => JNum(v)

      case  DateConstant(d) => JStr(d.toString)

      case  StringConstant(s) => JStr(s)

      case  EmptyConstant => JNull

      case Variable(n) => JStr(n)

    }
  }

}
