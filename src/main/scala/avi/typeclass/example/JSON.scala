package avi.typeclass.example

sealed trait JSON
case class JSeq(elms:List[JSON]) extends JSON
case class JObj(bindings:Map[String,JSON]) extends JSON
case class JNum(num:Double) extends JSON
case class JStr(str:String) extends JSON
case class JBool(b:Boolean) extends JSON
case object JNull extends JSON


object JSONWriter{
  def write(j:JSON):String = {
    j match {
      case JSeq(e) => e.map(write).mkString("[",",","]")
      case JObj(obj) => val x = obj.map(o=> "\""+o._1+"\":"+write(o._2))
        x.mkString("{",",","}")
      case JNum(n) => n.toString
      case JStr(s) => "\""+s+"\""
      case JBool(b) => b.toString
      case JNull => "null"

    }
  }
  def write[A](value:A)(implicit j:JSONSerializer[A]):String = write(j.toJSON(value))

  def toJSONString[A:JSONSerializer](value:A):String = {
    val j = implicitly[JSONSerializer[A]]//pull the implicit def of JSONSerializer
    write(j.toJSON(value))
    //we can replace the upper two lines with:  write(implicitly[JSONSerializer[A]].toJSON(value))
  }
}

trait JSONSerializer[T] {
  def toJSON(value:T):JSON

}


