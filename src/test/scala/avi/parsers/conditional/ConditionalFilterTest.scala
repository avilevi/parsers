package avi.parsers.conditional



import org.scalatest._


class ConditionalFilterTest extends  FlatSpec  with ShouldMatchers{

  def getRecord(fields:Map[String,String]) = Record(fields)
  def getFilter(expr:String ) = new ConditionalFilter(expr)

  "Filter by col_name with rule " should "IS NULL include records with null values " in{
    val f = getFilter("foo IS NULL" )
    val f2 = getFilter("foo IS NOT NULL" )
    val r = getRecord(Map("foo"-> null,"bar"-> "5.2"))
    f.filter(r) should equal( Some(r))
    f2.filter(r) should equal( None)

  }
  it should "IS NOT NULL remove records with null values " in{
    val f = getFilter("foo IS NOT NULL" )
    val f2 = getFilter("foo IS NULL" )
    val r = getRecord(Map("foo"-> "foo","bar"-> "5.2"))
    f.filter(r) should equal( Some(r))
    f2.filter(r) should equal( None)

  }
  it should "= output records that equal the value" in {
    val f = getFilter("foo = 'FOO'" )
    val f2 = getFilter("foo = 'FOOx'" )
    val r = getRecord(Map("foo"-> "FOO","bar"-> "5.2"))
    f.filter(r) should equal( Some(r))
    f2.filter(r) should equal( None)
  }
  it should "= validate without escaping" in {
    val f = getFilter("foo = 'FOO'" )
    val f2 = getFilter("foo = 'FOOx'" )
    val r = getRecord(Map("foo"-> "FOO","bar"-> "5.2"))

    f.filter(r) should equal( Some(r))
    f2.filter(r) should equal( None)
  }
  it should "= words with space" in {
    val f = getFilter("foo = 'FOO ME'" )
    val f2 = getFilter("foo = 'FOOx'" )
    val r = getRecord(Map("foo"-> "FOO ME","bar"-> "5.2"))
    f.filter(r) should equal( Some(r))
    f2.filter(r) should equal( None)
  }


  it should "!= output records that NOT equal the value" in {
    val f = getFilter("foo != 'FOOx'" )
    val f2 = getFilter("foo != 'FOO'" )
    val r = getRecord(Map("foo"-> "FOO","bar"-> "5.2"))

    f.filter(r) should equal( Some(r))
    f2.filter(r) should equal( None)
  }
  it should ">= output records that >= the value" in {
    val f = getFilter("bar >= 5.2" )
    val f2 = getFilter("bar >= 30" )
    val r = getRecord(Map("bar"-> "5.2"))
    f.filter(r) should equal( Some(r))
    f2.filter(r) should equal( None)
  }

  it should "<= output records that <= the value" in {
    val f = getFilter("bar <= 3" )
    val f1 = getFilter("bar <= 3.5" )
    val f2 = getFilter("bar <= 0" )
    val r = getRecord(Map("bar"-> "3"))

    f.filter(r) should equal( Some(r))
    f1.filter(r) should equal( Some(r))
    f2.filter(r) should equal( None)
  }

  it should "> output records that < the value" in {
    val f = getFilter("bar < 3" )
    val f1 = getFilter("bar < 1.5" )
    val r = getRecord(Map("bar"-> "2"))
    f.filter(r) should equal( Some(r))
    f1.filter(r) should equal( None)
  }
  it should "negative > output records that < the value" in {
    val f = getFilter("bar < 0" )
    val f1 = getFilter("bar < -5" )
    val r = getRecord(Map("bar"-> "-2"))

    f.filter(r) should equal( Some(r))
    f1.filter(r) should equal( None)
  }

  it should "> output records that > the value" in {
    val f = getFilter("bar > 5" )
    val f2 = getFilter("bar >= 30" )
    val r = getRecord(Map("bar"-> "5.2"))

    f.filter(r) should equal( Some(r))
    f2.filter(r) should equal( None)
  }

  "Filter with multi conditions with operator" should " be true only if both sides of AND condition are true" in{
    val f = getFilter("foo = 1 AND foo = 1")
    val f1 = getFilter("foo = 2 AND foo = 1")
    val r = getRecord(Map("foo"-> "1"))

    f.filter(r) should equal( Some(r))
    f1.filter(r) should equal( None)

  }
  it should "be true only if at least one side of OR condition is true" in{
    val f = getFilter("foo = 1 OR bar = 1")
    val f1 = getFilter("buz = 2 OR bar = 1")
    val f2 = getFilter("foo = 1 OR bar = 3")
    val f3 = getFilter("foo = 3 OR bar = 1")

    val r = getRecord(Map("foo"-> "1","bar"-> "3","buz" -> "2"))

    f.filter(r) should equal( Some(r))
    f1.filter(r) should equal( Some(r))
    f2.filter(r) should equal( Some(r))
    f3.filter(r) should equal( None)

  }

  it should "test complex expression" in{
    val f = getFilter("foo = 1 OR bar = 1 AND buz = 2")
    val f1 = getFilter("foo = 3 OR ( bar = 1 AND buz = 2 )")
    val f2 = getFilter("( foo = 1 OR bar = 1 OR buz = 2 )")
    val f3 = getFilter("( foo = 1 AND bar = 1 ) OR buz = 2 ")
    val r = getRecord(Map("foo"-> "1","bar"-> "3","buz" -> "2"))

    f.filter(r) should equal( Some(r))
    f1.filter(r) should equal( None)
    f2.filter(r) should equal( Some(r))
    f3.filter(r) should equal( Some(r))

  }



  it should "have the same result using AND or && " in{
    val f = getFilter("foo = 1 AND foo = 1")
    val fa = getFilter("foo = 1 && foo = 1")
    val f1 = getFilter("foo = 2 AND foo = 1")
    val f1a = getFilter("foo = 2 && foo = 1")
    val r = getRecord(Map("foo"-> "1"))

    f.filter(r) should equal( fa.filter(r))
    f1.filter(r) should equal( f1a.filter(r))

  }
  it should "have the same result using OR or || " in{
    val f = getFilter("foo = 1 OR bar = 1")
    val fa = getFilter("foo = 1 || bar = 1")
    val f1 = getFilter("buz = 2 OR bar = 1")
    val f1a = getFilter("buz = 2 || bar = 1")
    val r = getRecord(Map("foo"-> "1","bar"-> "3","buz" -> "2"))

    f.filter(r) should equal( fa.filter(r))
    f1.filter(r) should equal( f1a.filter(r))

  }
  "Filter errors " should "If referring an unknown column name" in{
    //msg: FilterType on table "table_name": referenced column "column_name" does not exist
  }
  it should "If referring a column with a data type that is not supported" in{
    //msg FilterType on table "table_name": filter on data type "type" is not supported, column name is "column_name"
  }
  it should "add others for malformed conditions for ease of human editing, for example" in {
    /*msg FilterType on table "table_name": missing right parenthesis after token "something"
    FilterType on table "table_name": too many right parenthesis after tokens "something something"
    FilterType on table "table_name": missing right value after "AND"
    */
  }

}
