package avi.parsers.conditional

import java.sql.{Timestamp, Date}
import java.text.SimpleDateFormat
import java.time.format.DateTimeFormatter

import avi.parsers.conditional.ASTObjects._
import org.scalatest.{ShouldMatchers, FlatSpec}
import FilterConstants._

import scala.util.Try

class ConditionParserTest extends FlatSpec with ShouldMatchers with ConditionParser{
  "Parser" should "Parse Null comparison" in {

    parse("foo IS NULL") should equal (Some(Comparison(Variable("foo"),IsNull)))
    parse("foo IS NOT NULL") should equal (Some(Comparison(Variable("foo"),IsNotNull)))
  }
  it should " parse left right comparison numeric values" in {
    parse("foo = 8") shouldEqual Some(LRComparison(Variable("foo"),Equals,NumericConstant(8D)))
    parse("foo = 8.5") shouldEqual Some(LRComparison(Variable("foo"),Equals,NumericConstant(8.5)))
    parse("foo = -8")shouldEqual Some(LRComparison(Variable("foo"),Equals,NumericConstant(-8D)))
  }
  it should " parse left right comparison String values" in {
    parse("foo = 'BAR'") shouldEqual Some(LRComparison(Variable("foo"),Equals,StringConstant("BAR")))
    parse("foo != 'BAR'") shouldEqual Some(LRComparison(Variable("foo"),NotEquals,StringConstant("BAR")))
  }
    it should " parse date values" in {
    parse("foo = '2015-03-25'") shouldEqual Some(LRComparison(Variable("foo"),Equals,DateConstant(simpleDateFormat.parse("2015-03-25").getTime)))
   }
    it should " parse date time values" in {
      val d1 = "'2015-03-25 14:15:16'"
      val d2 = "'2015-03-25 14:15:16.789'"
    parse(s"foo = $d1 ") shouldEqual Some(LRComparison(Variable("foo"),Equals,DateConstant(shortDateTimeFormat.parse(d1.replaceAll("'","")).getTime)))
    parse(s"foo = $d2 ") shouldEqual Some(LRComparison(Variable("foo"),Equals,DateConstant( longDateTimeFormat.parse(d2.replaceAll("'","")).getTime)))
   }
}
